# Jimmy Stewart Tribute Page

## About

This is a tribute page for the actor Jimmy Stewart. It is written
solely in HTML and CSS.

[Click here](https://emerac.gitlab.io/jimmy-stewart-tribute-page) to
view this page!

This page generally follows the requirements as laid out in
freeCodeCamp's Responsive Web Design Course, Project 1. However,
I chose not to follow the requirements exactly so that I could
exercise more creativity.

All images are used in accordance with their respective licenses.

## License

This project is licensed under the GNU General Public License.
For the full license text, view the [LICENSE](LICENSE) file.

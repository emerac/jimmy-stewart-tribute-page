# Images

"[Alexander Stewart hardware store](https://commons.wikimedia.org/wiki/File:Alexander_Stewart_hardware_store.jpg)"
by an
unknown author
is in the
[public domain](https://en.wikipedia.org/wiki/Public_domain)

"[It's a Wonderful Life (1946 poster)](https://commons.wikimedia.org/wiki/File:It%27s_a_Wonderful_Life_(1946_poster).jpeg)"
by
[RKO Radio Pictures Inc.](https://en.wikipedia.org/wiki/RKO_Pictures)
is in the
[public domain](https://en.wikipedia.org/wiki/Public_domain)

"[James Stewart & Wendy Barrie Speed (1936) Still Poster](https://commons.wikimedia.org/wiki/File:James_Stewart_%26_Wendy_Barrie_Speed_(1936)_Still_Poster.jpg)"
by
Metro-Goldwyn-Mayer
is in the
[public domain](https://en.wikipedia.org/wiki/Public_domain)

"[James Stewart - 1959](https://commons.wikimedia.org/wiki/File:James_Stewart_-_1959.jpg)"
by
Columbia Pictures
is in the
[public domain](https://en.wikipedia.org/wiki/Public_domain)

"[James Stewart by Irving Lippman, 1938](https://commons.wikimedia.org/wiki/File:James_Stewart_by_Irving_Lippman,_1938.jpg)"
by
Irving Lippman
is in the
[public domain](https://en.wikipedia.org/wiki/Public_domain)

"[James Stewart in Mr. Smith Goes to Washington (1939)](https://commons.wikimedia.org/wiki/File:James_Stewart_in_Mr._Smith_Goes_to_Washington_(1939)_(cropped).jpg)"
by
Columbia Pictures
is in the
[public domain](https://en.wikipedia.org/wiki/Public_domain)

"[Jimmy Stewart](https://commons.wikimedia.org/wiki/File:Jimmy_Stewart.jpg)"
by
[Carl Van Vechten](https://en.wikipedia.org/wiki/Carl_Van_Vechten)
is in the
[public domain](https://en.wikipedia.org/wiki/Public_domain)

"[Jimmy Stewart as Lindbergh with the Spirit replica for the movie The Spirit of St. Louis](https://commons.wikimedia.org/wiki/File:Jimmy_Stewart_as_Lindbergh_with_the_Spirit_replica_for_the_movie_The_Spirit_of_St._Louis..jpg)"
from the
[SDASM Archives](https://www.flickr.com/people/49487266@N07)
is in the
[public domain](https://en.wikipedia.org/wiki/Public_domain)

"[Jimmy Stewart getting medal](https://commons.wikimedia.org/wiki/File:Jimmy_Stewart_getting_medal.jpg)"
by an
unknown author
is in the
[public domain](https://en.wikipedia.org/wiki/Public_domain)

"[Maj. Jimmy Stewart](https://commons.wikimedia.org/wiki/File:Maj._Jimmy_Stewart.jpg)"
by the
U.S. Air Force
is in the
[public domain](https://en.wikipedia.org/wiki/Public_domain)

"[Rearwindow trailer 1](https://commons.wikimedia.org/wiki/File:Rearwindow_trailer_1.jpg)"
from a
trailer screenshot
is in the
[public domain](https://en.wikipedia.org/wiki/Public_domain)

"[Shop Around the Corner](https://commons.wikimedia.org/wiki/File:Shop-Around-the-Corner.jpg)"
by
Metro-Goldwyn-Mayer
is in the
[public domain](https://en.wikipedia.org/wiki/Public_domain)

"[Stewart and 'Male Call' Crew](https://www.americanairmuseum.com/media/9370)"
provided by
[The American Air Museum](https://www.americanairmuseum.com/)
is licensed under
[CC-BY-NC](https://creativecommons.org/licenses/by-nc/3.0/legalcode)

"[The Great American Bachelor](https://commons.wikimedia.org/wiki/File:Annex_-_Stewart,_James_(Call_Northside_777)_01.jpg)"
from a
studio publicity still
is in the
[public domain](https://en.wikipedia.org/wiki/Public_domain)

"[Wagner and Stewart at Relay Marathon](https://www.robert-wagner.com/jimmy-stewart-marathon/)"
(converted to greyscale) sourced from
[Robert Wagner: The Official Website](https://www.robert-wagner.com/)
is used in accordance with the terms of
[Fair Use](https://www.copyright.gov/fair-use/more-info.html)
due to the absence of a posted license

"[Yellow Jack Play 1934](https://commons.wikimedia.org/wiki/File:Yellow_Jack_Play_1934.jpg)"
by an
unknown author
is in the
[public domain](https://en.wikipedia.org/wiki/Public_domain)
